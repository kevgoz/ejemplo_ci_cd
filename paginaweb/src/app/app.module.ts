import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrarUsrComponent } from './registrar-usr/registrar-usr.component';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    // SE ACREGO DOS COMPONENTES
    RegistrarUsrComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // SE AGREGO PARA TRABAJAR CON EL formGROUP
    ReactiveFormsModule,
    // SE AGREGO ROUTERMoDULE PARA ENTRAR A MIS DIRECCIONES DE LOS COMPOENTES
    // EXPLICACION ROUTER MODULE
    //https://angular.io/guide/router-tutorial
    RouterModule.forRoot([
      {path: 'app-login', component: LoginComponent},
      {path: 'app-registrar-usr', component: RegistrarUsrComponent},
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
