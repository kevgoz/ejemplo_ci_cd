import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarUsrComponent } from './registrar-usr.component';

describe('RegistrarUsrComponent', () => {
  let component: RegistrarUsrComponent;
  let fixture: ComponentFixture<RegistrarUsrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarUsrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarUsrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
